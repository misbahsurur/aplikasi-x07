package surur.misbahus.appx07

import android.content.DialogInterface
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.google.zxing.BarcodeFormat
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var intentIntegrator: IntentIntegrator
    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var builder : AlertDialog.Builder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        intentIntegrator = IntentIntegrator(this)
        btnGenerateQr.setOnClickListener(this)
        btnScanQr.setOnClickListener(this)
        db = DBOpenHelper(this).writableDatabase
        btnSimpan.setOnClickListener(this)
        builder = AlertDialog.Builder(this)
    }

    fun getDBObject() : SQLiteDatabase{
        return db
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnScanQr -> {
                intentIntegrator.setBeepEnabled(true).initiateScan()
            }
            R.id.btnGenerateQr -> {
                val barCodeEncoder = BarcodeEncoder()
                val bitmap = barCodeEncoder.encodeBitmap(edQrCode.text.toString(),
                    BarcodeFormat.QR_CODE,400,400)
                imV.setImageBitmap(bitmap)
            }
            R.id.btnSimpan ->{
                builder.setTitle("Konfirmasi").setMessage("Apakah anda yakin?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        val intentResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data)
        if (intentResult!=null){
            if (intentResult.contents != null){
                edQrCode.setText(intentResult.contents)
                val token = StringTokenizer(edQrCode.text.toString(),";",false)
                edNim.setText(token.nextToken())
                edNama.setText(token.nextToken())
                edProdi.setText(token.nextToken())
            }else{
                Toast.makeText(this,"Dibatalkan",Toast.LENGTH_SHORT).show()
            }
        }
            super.onActivityResult(requestCode, resultCode, data)

    }

    fun showDataMhs(){
        val cursor : Cursor = db.query("bio", arrayOf("nim as _id", "nama", "prodi" ),
            null, null, null,null,"nama asc")
        adapter = SimpleCursorAdapter(this,R.layout.item_bio,cursor,
            arrayOf("_id","nama","prodi"), intArrayOf(R.id.tx_nim, R.id.tx_nama, R.id.tx_prodi),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lsBio.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        showDataMhs()
    }

    fun insertDataMhs(nim : String, nama : String , prodi : String){
        var sql = "insert into bio (nim,nama,prodi) values (?,?,?)"
        db.execSQL(sql, arrayOf(nim,nama,prodi))
        showDataMhs()
    }
    val btnInsertDialog = DialogInterface.OnClickListener{ dialog, which ->
        insertDataMhs(edNim.text.toString(), edNama.text.toString(), edProdi.text.toString())
        edNim.setText("")
        edNama.setText("")
        edProdi.setText("")
        Toast.makeText(this,"Data berhasil ditambah",Toast.LENGTH_SHORT).show()
    }

}
