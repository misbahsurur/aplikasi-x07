package surur.misbahus.appx07

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context): SQLiteOpenHelper(context, DB_Name, null, DB_Ver) {
    companion object{
        val DB_Name = "hotel"
        val DB_Ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tBio = "create table bio(nim text primary key, nama text not null, prodi text not null)"
        db?.execSQL(tBio)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS bio")
        onCreate(db)
    }
}